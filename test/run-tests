#!/bin/bash

set -eu

STATE="$(mktemp -d)"

# top-dir of test suite
TESTS="test"

#shellcheck disable=SC2064 # we want to expand variables now
trap "rm -rf '$STATE'" 0 INT QUIT ABRT PIPE TERM

chown logcheck:logcheck "$STATE"
chmod 0750 "$STATE"


STATUS="PASS"

# usage: run_test "name of test - description" \
#	./expected_output.file <expected exit status> \
#	command_to_test arg1 arg2...
# The global variable "$STATUS" is set to "FAIL" if this test fails
run_test(){
	local name="$1"
	local expected_file="$2"
	local expected_exit="$3"
	shift 3
	local my_status=""
	local diff="" code="0"

	"$@" > ./actual_file 2>&1 || code="$?"

	# ensure error output is reproducible
	sed -e 's|/tmp/logcheck\......./|/TMPDIR/|;/Re-run logcheck with the /Q' \
		./actual_file > ./actual_file.filtered  2>&1 ||:

	diff="$(diff -u -- "$expected_file" ./actual_file.filtered 2>&1 || :)"

	if [ "$code" != "$expected_exit" ]; then
		my_status="ERROR (expected exit: $expected_exit, actual: $code)"
	elif [ -z "$diff" ]; then
		my_status="PASS"
	else
		my_status="FAIL"
	fi

	echo "** $my_status: $name"
	if [ "$my_status" != "PASS" ]; then
		STATUS=FAIL
		cat <<EOF

== [ EXPECTED: $name ] ======
$(< "$expected_file")
=============================

== [ ACTUAL: $name ] ========
$(< ./actual_file.filtered)
=============================

== [ DIFF: $name ] ==========
$diff
=============================

*** raw output follows
== [ ACTUAL (raw): $name ] ========
$(< ./actual_file)
=============================


EOF
	fi
}

# for debugging only
Xrun_test(){
	#shellcheck disable=SC2317
	echo "** DISABLED: $1"
}

# ensure our mocked out version of mime-construct is run
export PATH="$TESTS/bin:$PATH"


if logcheck | grep 'logcheck should not be run as root'; then
	echo "** PASS: fails if run as root"
else
	echo "** FAIL: missing error if running as root"
	STATUS=FAIL
fi

test=00--no-logfiles-list
run_test "$test: Error if -L /no-such-list" $TESTS/$test/expected 1 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -L /no-such-list --logfiles-list-directory /dev/null -S '$STATE' -c $TESTS/$test/config -r $TESTS/$test" \
		logcheck

test=00--unreadable-log
run_test "$test: Warning if -L points to unreadable log" $TESTS/$test/expected 1 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o --logfiles-list $TESTS/$test/list -D /dev/null -S $STATE $TESTS/$test/config -r $TESTS/$test" \
		logcheck

test=00--unreadable-log-on-cmdline
run_test "$test: Error if -l points to unreadable log" $TESTS/$test/expected 1 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o --logfile /none -S $STATE $TESTS/$test/config -r $TESTS/$test" \
		logcheck

test=00--infinite-loop-detection
run_test "$test: Infinite looks in rules.enabled.d should be detected" $TESTS/$test/expected 1 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o --logfile $TESTS/$test/log --rules-directory $TESTS/$test -c $TESTS/$test/config -S '$STATE'" \
		logcheck


test=00--unreadable-config
run_test "$test: Unreadable config should be detected" $TESTS/$test/expected 1 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -c /no.conf -S $STATE -r $TESTS/$test -l $TESTS/$test/log" \
		logcheck

## Testing of output
test=01--basic-output
subtest=01a--no-header
run_test "$test ($subtest 1/2)" \
	$TESTS/$test/$subtest/expected-1 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/list -D /dev/null -c $TESTS/$test/$subtest/config -r $TESTS/$test/rulefiles" \
		logcheck

# expect no output
run_test "$test ($subtest 2/2)" \
	$TESTS/$test/$subtest/expected-2-empty 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/list -D /dev/null -c $TESTS/$test/$subtest/config -r $TESTS/$test/rulefiles" \
		logcheck
rm -f "$STATE"/offset*

subtest=01b--with-header
run_test "$test ($subtest 1/2)" \
	$TESTS/$test/$subtest/expected-1 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/list -D /dev/null -c $TESTS/$test/$subtest/config -r $TESTS/$test/rulefiles" \
		logcheck

# expect empty output
run_test "logcheck ($subtest 2/2)" \
	$TESTS/$test/$subtest/expected-2-empty 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/list -D /dev/null -c $TESTS/$test/$subtest/config -r $TESTS/$test/rulefiles" \
		logcheck
rm -f "$STATE"/offset*

subtest=01c--INTRO-yes
run_test "$test ($subtest: INTRO=yes is the same as INTRO=1 - both enable the intro)" \
	$TESTS/$test/$subtest/expected-1 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/list -D /dev/null -c $TESTS/$test/$subtest/config -r $TESTS/$test/rulefiles" \
		logcheck
rm -f "$STATE"/offset*

subtest=01d--INTRO-no
run_test "logcheck ($subtest: INTRO=no is treated the same as disabling the intro)" \
	$TESTS/$test/$subtest/expected-1 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/list -D /dev/null -c $TESTS/$test/$subtest/config -r $TESTS/$test/rulefiles" \
		logcheck
rm -f "$STATE"/offset*

test=02--highlights
subtest=2a
run_test "$test ($subtest - just one line is flagged)" \
	$TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/logfiles -D /dev/null -c $TESTS/$test/config -r $TESTS/$test/$subtest/rulefiles--highlights-1" \
		logcheck
rm -f "$STATE"/offset*

subtest=2b
run_test "$test ($subtest - both lines are flagged as important)" \
	$TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/logfiles -D /dev/null -c $TESTS/$test/config -r $TESTS/$test/$subtest/rulefiles--highlights-2" \
		logcheck
rm -f "$STATE"/offset*

subtest=2c
run_test "$test ($subtest - subdir is merged)" \
	$TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/logfiles -D /dev/null -c $TESTS/$test/config -r $TESTS/$test/$subtest/rulefiles--highlights-3" \
		logcheck
rm -f "$STATE"/offset*

test=03--enabling-rules
subtest=3a
run_test "$test ($subtest - paranoid only (via symlinks, plus no highlight.enabled.d dir)" \
	$TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/logfiles -D /dev/null -c $TESTS/$test/config -r $TESTS/$test/$subtest/rulefiles--paranoid-only" \
		logcheck
rm -f "$STATE"/offset*

subtest=3b
run_test "$test ($subtest - server and paranoid (and empty highlight.enabled.d))" \
	$TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/logfiles -D /dev/null -c $TESTS/$test/config -r $TESTS/$test/$subtest/rulefiles--server" \
		logcheck
rm -f "$STATE"/offset*

subtest=3c
run_test "$test ($subtest - server+paranoid+workstation (and symlinked hilight.enabled.d))" \
	$TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -o -S '$STATE' -L $TESTS/$test/logfiles -D /dev/null -c $TESTS/$test/config -r $TESTS/$test/$subtest/rulefiles--all-enabled" \
		logcheck
rm -f "$STATE"/offset*
# TODO: error message /warning if -r points to non-existant dir

test="04--macros"
subtest=""
run_test "$test: expanding macros 1 (expand-macros.awk)" $TESTS/$test/expected 0 \
	gawk -f "/usr/share/logcheck/expand-macros.awk" \
		$TESTS/$test/test.rules

run_test "$test: expanding macros 2 (with default.macros)" $TESTS/$test/expected-with-default 0 \
	gawk -f "/usr/share/logcheck/expand-macros.awk" \
		/etc/logcheck/macros/default.macros \
		$TESTS/$test/test.rules

run_test "$test: expanding macros 3 (using default.macros and local.macros)" $TESTS/$test/expected-with-local 0 \
	gawk -f "/usr/share/logcheck/expand-macros.awk" \
		/etc/logcheck/macros/default.macros \
		$TESTS/$test/local.macros \
		$TESTS/$test/test.rules

# TODO: warning if default.macros does not exist
# TODO: test macros cant cause an ininite floop

test=05--actual-rules

# (nb: by default, duplicate log entries are not removed)
subtest="5a"
run_test "$test: $subtest - actual rules and config" $TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c "/usr/sbin/logcheck -o -l $TESTS/$test/log -S '$STATE'" logcheck
rm -f "$STATE"/offset*

subtest=0
run_test "$test: $subtest - testing we mocked out mime-construct OK" \
	$TESTS/$test/$subtest/expected 0 \
	mime-construct --arg two

subtest=5b
run_test "$test: $subtest - no '-o'" $TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c "/usr/sbin/logcheck -l $TESTS/$test/log -S '$STATE'" logcheck
rm -f "$STATE"/offset*

subtest=5c
run_test "$test: $subtest - no -o and --cron" $TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c "/usr/sbin/logcheck --cron -l $TESTS/$test/log -S '$STATE'" logcheck
rm -rf "$STATE"/offset*

################
test=06--prioritisation
subtest=6a--run-1
# initial run, rules run in order: 1,2,3
su -s /bin/bash -c \
	"/usr/sbin/logcheck --stdout -l $TESTS/$test/log -r $TESTS/$test/rulefiles--priority -S '$STATE' -c $TESTS/$test/config" logcheck
rm -f "$STATE"/offset*

run_test "$test: $subtest - order should be updated" \
	$TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c "/usr/sbin/logcheck --list-rules -r $TESTS/$test/rulefiles--priority -S '$STATE' -c $TESTS/$test/config" logcheck

# after updating scores, rules run in better order: 3,1,2 -- and 0 is never run
su -s /bin/bash -c \
	"/usr/sbin/logcheck -l $TESTS/$test/log -r $TESTS/$test/rulefiles--priority -S '$STATE' -c $TESTS/$test/config" logcheck
rm -f "$STATE"/offset*

subtest=6b--run-2
run_test "$test: $subtest - order should be updated again" \
	$TESTS/$test/$subtest/expected 0 \
	su -s /bin/bash -c "/usr/sbin/logcheck --list-rules -r $TESTS/$test/rulefiles--priority -S '$STATE' -c $TESTS/$test/config" logcheck

subtest=6c--1-update-ranks
run_test "$test: $subtest - update ranks" \
	$TESTS/$test/$subtest/3-update.expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -l $TESTS/$test/log -r $TESTS/$test/rulefiles--priority -S '$STATE' -c $TESTS/$test/config --update-ranks" \
		logcheck

subtest=6c--2-list
run_test "$test: $subtest - order should be updated" \
	$TESTS/$test/$subtest/3.expected 0 \
	su -s /bin/bash -c "/usr/sbin/logcheck --list-rules -r $TESTS/$test/rulefiles--priority -S '$STATE' -c $TESTS/$test/config" logcheck

subtest=6c--3-next-update
run_test "$test: $subtest - rerun update" \
	$TESTS/$test/$subtest/4-update.expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck --logfile $TESTS/$test/log -r $TESTS/$test/rulefiles--priority --state-directory '$STATE' --update-ranks" logcheck

subtest=6c--4-list
run_test "$test: $subtest - order should be averaged" \
	$TESTS/$test/$subtest/4.expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck --list-rules --rules-directory $TESTS/$test/rulefiles--priority -S '$STATE' -c $TESTS/$test/config" logcheck

################
test=07--pre-processing
subtest=""
run_test "$test: lines with spaces" \
	$TESTS/$test/expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck --config $TESTS/$test/logcheck.conf -l $TESTS/$test/log -S '$STATE' --rules-directory $TESTS/$test -o" logcheck
rm -f "$STATE"/offset*

################
test=08--errors-in-rules
subtest=""
run_test "$test: errors in rules" \
	$TESTS/$test/expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -S $STATE -c $TESTS/$test/config -r $TESTS/$test -o -l $TESTS/$test/log" logcheck
rm -f "$STATE"/offset*

################
test=09--post-processing
subtest="missing script"
run_test "$test: post-processing: $subtest" \
	$TESTS/$test/expected-err 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -S $STATE -c $TESTS/$test/config-err -r $TESTS/$test -o -l $TESTS/$test/log" logcheck
rm -f "$STATE"/offset*

subtest="valid post-processor"
run_test "$test: post-processing: $subtest" \
	$TESTS/$test/expected 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -S $STATE -c $TESTS/$test/config -r $TESTS/$test -o -l $TESTS/$test/log" logcheck
rm -f "$STATE"/offset*

################
test=10--split-large-reports
subtest="splitting reports at MAX_LINES (2 sent)"
run_test "$test: $subtest" \
	$TESTS/$test/expected-2 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -S $STATE -c $TESTS/$test/config -r $TESTS/$test -l $TESTS/$test/log-2" logcheck
rm -f "$STATE"/offset*


subtest="splitting reports at MAX_LINES (3 sent)"
run_test "$test: $subtest" \
	$TESTS/$test/expected-3 0 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -S $STATE -c $TESTS/$test/config -r $TESTS/$test -l $TESTS/$test/log-3" logcheck
rm -f "$STATE"/offset*

subtest="aborting if more than MAX_EMAILS"
run_test "$test: $subtest" \
	$TESTS/$test/expected-err 1 \
	su -s /bin/bash -c \
		"/usr/sbin/logcheck -S $STATE -c $TESTS/$test/config -r $TESTS/$test -l $TESTS/$test/log-huge" logcheck
rm -f "$STATE"/offset*






if [ "$STATUS" = "PASS" ]; then
	echo "* $0: PASS"
	exit 0
else
	echo "* $0: $STATUS"
	exit 1
fi
