#!/bin/sh
set -e

# block relating to header.txt can be removed once trixie is stable (see also postinst)
if [ "$1" = "install" ] || [ "$1" = "upgrade" ]; then
	# preserve deletion of /etc/logcheck/header.txt if upgrading from <= 1.4.1 --- see #1049412
	if [ "$1" = "upgrade" ] && [ ! -f /etc/logcheck/header.txt ] && dpkg --compare-versions "$2" le-nl 1.4.1; then
		echo "You deleted /etc/logcheck/header.txt before bookworm: this deletion will be preserved. However, it would be better to set INTRO=0 in /etc/logcheck.conf instead."
		touch /etc/logcheck/header.txt.was-deleted-before-bookworm || :
	fi

	if dpkg --compare-versions "$2" lt-nl "1.4.3~" ; then
		# Update /etc/logcheck/header.txt if it matches a known
		# shipped version (woody to jessie) - see #1039591
		# (this can be removed once trixie is stable)
		if [ -f "/etc/logcheck/header.txt" ] && [ -f "/usr/share/logcheck/header.txt" ]; then
			# 1bc54d3bfb0d1e61104d5780a318ced2 (woody)
			# dbc1e8d136180d247b572f6a19c4e92e (lenny)
			# a32fc12d69628d96756fd3af3f8b3ecd (squeeze, wheezy, jessie)
			md5=$(md5sum "/etc/logcheck/header.txt" | awk '{print $1}')
			if [ "$md5" = "dbc1e8d136180d247b572f6a19c4e92e" ] ||
				 [ "$md5" = "a32fc12d69628d96756fd3af3f8b3ecd" ] ||
				 [ "$md5" = "1bc54d3bfb0d1e61104d5780a318ced2" ]; then
				cp -p -v /usr/share/logcheck/header.txt /etc/logcheck
			fi
		fi
	fi
fi

### migrate rules locations from pre-trixie 'REPORTLEVEL' to the new
### layout. Can be removed once forky is stable".
if [ "${1-}" = "upgrade" ]; then
	if dpkg --compare-versions "$2" lt-nl "1.5.0" ; then
		echo "Updating rules locations -- see /usr/share/logcheck/NEWS.Debian.gz"
		old_level=$(gawk -F "(=|='|=\"|'|\")" '$1=="REPORTLEVEL"{print $2}' \
			2>/dev/null /etc/logcheck/logcheck.conf ||:)
		echo "Preparing to migrate old REPORTLEVEL settings. You previously had: REPORTLEVEL=<$old_level>"
		flag_file=/etc/logcheck/previous-reportlevel.delete
		case "$old_level" in
			server|paranoid|workstation) echo "$old_level" > $flag_file  ;;
			*)
				echo "Your REPORTLEVEL setting was invalid. You will end up with the default rules enabled ('paranoid' and 'server')"
				echo "server" > $flag_file;;
		esac
	fi
fi

#DEBHELPER#
