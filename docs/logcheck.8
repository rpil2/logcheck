.TH logcheck 8 "Mar 28, 2024"
.SH NAME
logcheck \- check the system log for unusual entries

.SH SYNPOSIS
\fBlogcheck\fR [ \fIOPTIONS\fR ]
.PP
Logcheck analyses the system log for unuexpected entries that could
indicate problems or security issues.

Log entries in the system log (produced by
.BR systemd-journald (8),
.BR rsyslog (1)
or another
.BR system\-log\-daemon )
are checked against a customisable database of regular expressions
(such as that provided by the
.B logcheck\-database
package) to identify routine messages: anything that is not considered
routine is reported to the system administrator.

.SH INVOKING
.B logcheck
will be run automatically every hour by
.BR systemd (1)
(or by
.BR cron (8)
on
.RB non\- systemd
systems).

It should only be run as the
.B logcheck
user. This can be achieved by running one of the following as
.BR root :
.TP
.B # su \-s /bin/bash \-c "/usr/sbin/logcheck\fI OPTIONS\fB" logcheck
which should always work

.TP
.B # systemctl start logcheck.service
which works if you use
.BR systemd ,
but requires any options to be specified in the configuration files

.TP
.B # sudo \-u logcheck\ logcheck \fIOPTIONS\fR
which needs
.BR sudo (8)
permissions

.SH OPTIONS
Configuration should be done by editing the files in
.B /etc/logcheck/logcheck.conf
but some options can also be set on the command line:

.TP
\fB\-d\fR, \fB\-\-debug\fR
show debug output

.TP
\fB\-t\fR, \fB\-\-test\fR
apply all rules and report results, but log entries will be
re\-checked on subsequent runs

.TP
\fB\-T\fR, \fB\-\-no\-cleanup\fR
do not remove the working directory
.RB ( $TMPDIR )
which is created to hold cleaned versions of rules and log messages

.TP
\fB\-o\fR, \fB\-\-stdout\fR
report to stdout (no email will be sent)

.TP
\fB\-m\fR, \fB\-\-mailto\fR \fIADDRESS\fR
email any output to the specified
.I ADDRESS
(the default is to send to the local
.B logcheck
user)

.TP
\fB\-l\fR, \fB\-\-logfile\fR \fILOG\fR
check the specified log file
.I FILE
(this overrides the
.B \-L
and
.B \-D
options)

.TP
\fB\-L\fR, \fB\-\-logfiles\-list\fR \fIFILE\fR
check all log files listed in
.I FILE
(default:
.BR /etc/logcheck/logcheck.logfiles )

.TP
\fB\-D\fR, \fB\-\-logfiles\-list\-directory\fR \fIDIR\fR
check all log files listed in
.IB DIR /*.logfiles
(default:
.BR /etc/logcheck/logcheck.logfiles.d )

.TP
\fB\-c\fR, \fB\-\-config\fR \fICONFIG\fR
read settings from
.I CONFIG
file (default:
.BR /etc/logcheck/logcheck.conf )

.TP
\fB\-r\fR, \fB\-\-rules\-directory\fR \fIDIR\fR
use rules in directory
.I DIR
(default:
.BR /etc/logcheck/rules.enabled )

.TP
\fB\-S\fR, \fB\-\-state\-directory\fR \fIDIR\fR
use
.I DIR
to store state information
(default:
.BR /var/lib/logcheck )

.TP
\fB\-\-list\-rules\fR
list the order in which rules will be used (no processing occurs)

.TP
\fB\-h\fR, \fB\-\-help\fR\fR
print usage information and exit

.TP
\fB\-v\fR, \fB\-\-version\fR
print version and exit

.SH CONFIGUTION FILE
Options can be set in the configuration file
.B /etc/logcheck/logcheck.conf
(which can itself be changed with the
.B \-\-config
option). The possible options are explained in comments in
.BR /etc/logcheck/logcheck.conf .

.SH LOG FILES
By default
.B logcheck
checks log entries from standard system logs, i.e.,
the
.B systemd
journal and, if it exists,
.BR /var/log/syslog .
You can check other logs by listing them in a file (with a
.B .logfiles
extension) in
.B /etc/logcheck/logfiles.d
(or via the \fB\-L\fR, \fB\-D\fR options).

.B logcheck
does not care what these log files contain, but each line is treated
as a separate log entry.

.PP
All log files being checked are concatenated together, preprocessed and sorted before
checking (see the
.B PRE_PROCESS_LOG_ENTRIES
and
.B SORTUNIQ
options in the configuration file). Only new log entries are
processed on each
.B logcheck
run.



.SH RULES
.B logcheck
rules are text files located in
.B /etc/logcheck/rules.enabled.d
(this can be changed with the
.B \-r
option or the
.B RULES_DIRECTORY
setting in the configution file).

.PP
Rules are text files. The filename must match
.BR [A-Za-z0-9_-]+ .
An optional extension
.B .rules
can be used: rules with this extension are pre-processed for macros (see below).

.PP
Each line in a rules file is a
.BR egrep (1)
regular expression (possibly extendable by macros: see below): any log
entries matching the rule are filtered out (not reported). Blank lines
and lines starting with a
.B #
are ignored.

.PP
The
.B logcheck-database
ships default rules files in three directories,
.BR /etc/logcheck/rules.available.d/ignore.d.paranoid ,
.BR /etc/logcheck/rules.available.d/ignore.d.server ,
and
.BR /etc/logcheck/rules.available.d/ignore.d.workstation .
The rules in these directories can be enabled by symlinking these
directories (or individual files) into
.BR /etc/logcheck/rules.enabled.d .
These names date from the early 2000s (or before), when it was assumed
that
.BR server s
would need fewer rules than end-user
.BR workstation s,
and that some so-called
.B paranoid
users would want to enable a very minimal set of rules that would
filter only a few log messages. However, a modern system produces a
huge number of log messages and there is perhaps little real
difference between a server and a workstation: you will likly need to
enable all of these rules and also spend time writing your own rules
to reduce the
.B logcheck
reports to a manageable size. Unfortunately it is simply not feasible
for Debian to provide a perfect set of rules that work for
everyone.

.PP
We recommend that you do not edit the rules shipped by Debian, but
that you put your own local rules in a separate directory in
.B /etc/logcheck/rules.available.d
and symlink it into
.BR /etc/logcheck/rules.enabled.d .


.PP
A separate set of rules in
.B /etc/logcheck/highlights.enabled.d
is applied to log entries that are about to be reported: anything
matching one of these highlight rules is regarded as especially
important and is shown at the top of the report. The idea is to use
these to highlight errors and critical failures.

.PP
.B logheck
attempts to rank rules by the number of times they match a log
entries, and apply more frequently-matching rules files first to more
quickly reduce the number of log entries. This uses a basic algorithm
based on a rolling average which is refreshed monthly via the
.B logcheck-update.timer
systemd timer.


.SH MACROS
If a rules file ends in the extension
.B .rules
then it is pre-processed for
.B macros.

.PP
A 'macro' is defined by wrting
.BI $ MACRO = value
at the start of a line. Each subsequent use of
.B $MACRO
will then be replaced by
.B value
in a rule defined later in a file. The
.B MACRO
part can only contain
.B A-Za-z
but the
.B value
can be anything (no syntax checking), including contain other
macros. However, expansion of macros is very basic: each macro is
expanded in a rule that it is used in, but not when defining other
macros. Expansion only happens in reverse alphabetical order, with
lowercase macros expanded before upper case ones. So a rules file
containing
.br
  $A=A
  $a=a
  $Z=[$A-$a]
  ^$Z+
.br
will define a single rule equal to
.B ^[A-$a]+
because the macros are expanded in the order
.BR $a , $Z , $A ,
and the
.B $a
does not exist in the expansion early enough to be seen.

.PP
Macros defined in a rules file are local to that file, and can be
redefined part-way through. The file
.B /etc/logcheck/macros/default.macros
is included at the start of every rule. Rather than edit this file, you can create and use
.BR /etc/logcheck/macros/local.macros .
The
.B default.macros
file defines
.B $X
(standing for 'prefiX') which expands to something matching a standard
timestamp. This includes another macro
.B $PROG
which you can define in each rules file to match a programme
name. This means that instead of having every single rule start with
the same prefix you can simply set
.B $PROG
to match the prgramme
name and then use
.BR $X .

.PP
For example, to ignore log entries that look like:
.br
  Feb 08 19:25:09 hostname foo[220]: starting bar.
  Feb 08 19:25:10 hostname foo[220]: stopping baz.
  May 20 23:25:10 hostname foo[220]: starting quuz.
.br
foo.rules might contain:
.br
  $PROG=foo
  $NAME=[a-z]+
  $X: (start|stopp)ing $NAME\.$
  # and any other rules you like
.br

.SH FILES
.TP
.B /etc/logcheck/
the main configuration directory: this is accessible only to the
.B logcheck
user because knowing the configuration might (potentially) aid an
attacker

.TP
.B /etc/logcheck/logcheck.conf
the main configuration file

.TP
.B /etc/logcheck/logcheck.logfiles
(deprecated) list of log files to check: use
.B /etc/logcheck/logcheck.logfiles.d
instead

.TP
.B /etc/logcheck/logcheck.logfiles.d/
directory containig lists of logfiles to check. By default this contains
.B /etc/logcheck/logcheck.logfiles.d/journal.logfiles
(telling logcheck to check the
.BR journal )
and
.B /etc/logcheck/logcheck.logfiles.d/syslog.logfiles
(telling logcheck to check
.BR /var/log/syslog )

.TP
.B /etc/logcheck/rules.enabled.d
rules that are used to process log files

.TP
.B /etc/logcheck/rules.available.d
rules that are avialable: to enable a rules file (or directory of
rules files), symlink it into
.B /etc/logcheck/rules.enabled.d

.TP
.B /etc/logcheck/highlight.enabled.d
rules used to highlight log entries as particularly important

.B /etc/logcheck/highlight.available.d
similar to
.B rules.available.d
but for the highlight rules

.TP
.B /etc/logcheck/highlight.enabled.d
similar to
.B highlight.available.d
but for the highlight rules

.TP
.B /etc/logcheck/macros/default.macros
default macros available in every
.B .rules
file

.TP
.B /etc/logcheck/macros/local.macros
local macros loaded at the start of every
.B .rules
file (after
.BR default.macros )


.TP
.B /var/spool/logcheck
.B $HOME
directory of the
.B logcheck
user, and contains state information about previous runs

.TP
.B /var/spool/logcheck/journal
the timestamp of this file indicates when the
.B journal
was last checked

.TP
.B /var/spool/logcheck/offset.var.log.syslog
the contents of this file indicates when
.B /var/log/syslog
was last checked (one such file is created for each log file checked)

.TP
.B /var/spool/logcheck/rank
directory of files indicating the order in which rules should be
run

.TP
.B /usr/share/logcheck/expand-macros.awk
.BR gawk (1)
script that expands macros in rules files


.SH SEE ALSO
.BR logtail2 (8),
and
.BR logcheck-test (1).

.SH AUTHOR
Logcheck was originally written by Craig Rowland and was part of the
Abacus Project of security tools, but has been rewritten by various
Debian contributors. Past and present authors and maintainers are
listed in
.BR /usr/share/doc/logcheck/copyright .
