#-*-awk-*-

# replacement happens in reverse alphabetical order, lower before upper
BEGIN {PROCINFO["sorted_in"]="@ind_str_desc"}

# {print("Considering line" NR " [" $0 "]")}

# skip comments and blank
/^#/   {next}
/^\s*$/{next}

# $macro=value
match($0,/^(\$[A-Za-z]+)=(.+)$/,m){
	key="\\" m[1] # escape leading '$'
	value=m[2]
	# print("* Setting",key,"to",value)
	subs[key]=value
	next
}


# use macros in any line with a $ in the middle
{
	if ($0~/\$./)
		for (key in subs){
			# print("* replacing:",key,"with",subs[key])
			gsub(key,subs[key],$0)
			# print("* got",$0)
		}
	print
}
