#!/usr/bin/make -f

CONFDIR = etc/logcheck
SBINDIR = usr/sbin
BINDIR  = usr/bin
LOGTAIL_SHAREDIR = usr/share/logtail/detectrotate
LOGCHECK_SHAREDIR= usr/share/logcheck
STATEDIR = var/lib/logcheck
LOCKDIR  = run/lock/logcheck
all:

install:
	# Create the directories
	install -m 750 -d $(DESTDIR)/$(CONFDIR)
	install -m 750 -d $(DESTDIR)/$(STATEDIR)
	install -d $(DESTDIR)/$(SBINDIR)
	install -d $(DESTDIR)/$(BINDIR)
	install -d $(DESTDIR)/$(LOCKDIR)
	install -d $(DESTDIR)/$(LOGCHECK_SHAREDIR)
	install -d $(DESTDIR)/$(LOGTAIL_SHAREDIR)

	# Install the scripts
	install -m 755 src/logcheck $(DESTDIR)/$(SBINDIR)/
	sed -i "s/^VERSION=unknown$$/VERSION=$$(dpkg-parsechangelog -S version)/" $(DESTDIR)/$(SBINDIR)/logcheck
	install -m 755 src/logtail $(DESTDIR)/$(SBINDIR)/
	install -m 755 src/logtail2 $(DESTDIR)/$(SBINDIR)/
	install -m 755 src/logcheck-test $(DESTDIR)/$(BINDIR)/
	install -m 644 src/expand-macros.awk $(DESTDIR)/$(LOGCHECK_SHAREDIR)/
	install -m 644 src/detectrotate/10-savelog.dtr $(DESTDIR)/$(LOGTAIL_SHAREDIR)/
	install -m 644 src/detectrotate/20-logrotate.dtr $(DESTDIR)/$(LOGTAIL_SHAREDIR)/
	install -m 644 src/detectrotate/30-logrotate-dateext.dtr $(DESTDIR)/$(LOGTAIL_SHAREDIR)/
	# config
	cp -aR etc/* $(DESTDIR)/$(CONFDIR)

clean:
	# scripts
	-rm -f  $(DESTDIR)/$(SBINDIR)/logcheck
	-rm -f  $(DESTDIR)/$(SBINDIR)/logtail
	-rm -f  $(DESTDIR)/$(SBINDIR)/logtail2
	-rm -f  $(DESTDIR)/$(BINDIR)/logcheck-test
	-rm -rf  $(DESTDIR)/$(LOGCHECK_SHAREDIR)
	-rm -rf  $(DESTDIR)/$(LOGTAIL_SHAREDIR)
	# config
	-rm -rf $(DESTDIR)/$(CONFDIR)
	# dirs for output
	-rmdir $(DESTDIR)/$(STATEDIR)
	-rmdir $(DESTDIR)/$(LOCKDIR)

distclean:
	-find . -name "*~" | xargs --no-run-if-empty rm -vf
