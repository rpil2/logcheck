$PROG=systemd
$UNIT=[^ ]+

## services, and other units
#   https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/job.c#L647 #L659 #587 #589
#   from: job_start_message_format, job_done_message_format
# not including: Reloaded/Reloading
$X: (Start|Stopp)ed .+\.$
$X: (Start|Stopp)ing .+\.$

# eg, units started by timers
#   https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/unit.c#6023
#   from: unit_log_success
$X: $UNIT: Deactivated successfully\.$

# possibly from: unit_notify, https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/unit.c/#L2706
$X: $UNIT: Succeeded\.$

# https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/unit.c/#L2350
# from: unit_log_resources, optional memory peak from memory_fields added from strextendf_with_separator at L2377
$X: $UNIT: Consumed .+ CPU time(, .+ memory peak)?\.$

# services with Type=Oneshot print 'Finished' on exit
# https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/service.c/#L5425
# from: service_finished_job
$X: Finished .+\.$

# services with Restart=always (eg console-getty.service)
# https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/service.c#L2680
# from: service_enter_restart
$X: $UNIT\.service: Scheduled restart job, restart counter is at [0-9]+\.$

## timers
# no longer produced normally?
#$X: $UNIT\.timer: Adding .+ random time\.$

## slices
# https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/slice.c#L428-431
# from: .status_message_formats in slice_vtable
$X: (Creat|Remov)ed slice .+\.$

## targets
# https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/target.c/#L201
# # from: .status_message_formats in target_vtable ('Stopped target xxx' already matched by first rule)
$X: Reached target .+\.$

# eg on logout from console
# https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/manager.c/#L3062
# from: manager_start_special
$X: Activating special unit exit\.target\.\.\.$

## sockets
# https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/socket.c/#L3626-3631
# from: .status_message_formats socket_vtable
$X: Listening on .+\.$
$X: Closed .+\.$

## The following seem to be produced only when systemd-sysv is installed (which is usually the case)
# https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/main.c/#L2605
# from: do_queue_default_job
$X: Queued start job for default target .+\.$

# https://salsa.debian.org/systemd-team/systemd/-/blob/debian/master/src/core/manager.c/#L3919-3949
# from: manager_notify_finished
$X: Startup finished in .+\.$
